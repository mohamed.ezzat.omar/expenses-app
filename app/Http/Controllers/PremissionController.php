<?php


namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{

    public function Permission()
    {
        $dev_permission = Permission::where('slug','create-tasks')->first();
        $manager_permission = Permission::where('slug', 'edit-users')->first();

        //RoleTableSeeder.php
        $dev_role = new Role();
        $dev_role->slug = 'employee';
        $dev_role->name = 'Accounting Member';
        $dev_role->save();
        $dev_role->permissions()->attach($dev_permission);

        $manager_role = new Role();
        $manager_role->slug = 'manager';
        $manager_role->name = 'CEO';
        $manager_role->save();
        $manager_role->permissions()->attach($manager_permission);

        $dev_role = Role::where('slug','employee')->first();
        $manager_role = Role::where('slug', 'manager')->first();

        $createTasks = new Permission();
        $createTasks->slug = 'create-expenses';
        $createTasks->name = 'Create Expenses';
        $createTasks->save();
        $createTasks->roles()->attach($dev_role);

        $editUsers = new Permission();
        $editUsers->slug = 'edit-expenses';
        $editUsers->name = 'Edit Expenses';
        $editUsers->save();
        $editUsers->roles()->attach($manager_role);

        $employee_role = Role::where('slug','developer')->first();
        $manager_role = Role::where('slug', 'manager')->first();
        $employee_perm = Permission::where('slug','create-expenses')->first();
        $manager_perm = Permission::where('slug','edit-users')->first();

        $developer = new User();
        $developer->name = 'Employee';
        $developer->email = 'harsukh21@gmail.com';
        $developer->password = bcrypt('harsukh21');
        $developer->save();
        $developer->roles()->attach($employee_role);
        $developer->permissions()->attach($employee_perm);

        $manager = new User();
        $manager->name = 'Manager';
        $manager->email = 'jitesh21@gmail.com';
        $manager->password = bcrypt('jitesh21');
        $manager->save();
        $manager->roles()->attach($manager_role);
        $manager->permissions()->attach($manager_perm);


        return redirect()->back();
    }
}
