<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses = Expense::all();

        return view('expenses.index', compact('expenses'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'attachment'=>'required',
            'amount'=>'required',
            'date'=>'required'
        ]);

        $fileName = $request->file('attachment')->getClientOriginalName();
        $request->file('attachment')->storeAs("attachments", $fileName);

        $expense = new Expense([
            'name' => $request->get('name'),
            'attachment' => $fileName,
            'amount' => $request->get('amount'),
            'date' => $request->get('date')
        ]);

        $expense->save();
        return redirect('/expenses')->with('success', 'Expense saved!');
    }

    public function create()
    {
        return view('expenses.create');
    }

    public function show($id)
    {
        $expense = Expense::find($id);
        return response()->json($expense);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name'=>'required',
            'attachment'=>'required',
            'amount'=>'required',
            'date'=>'required'
        ]);

        Expense::find($id)->update(['name'=>$request->get('name'),
                                               'attachment' => $request->get('attachment'),
                                               'amount' => $request->get('amount'),
                                               'date' => $request->get('date')
        ]);

        return redirect('/expenses')->with('success', 'Expense updated!');
    }

    public function edit($id)
    {
        $expense = Expense::find($id);
        return view('expenses.edit', compact('expense'));
    }

    public function destroy($id)
    {
        Expense::find($id)->delete();

        return redirect('/expenses')->with('success', 'Expense Deleted!');
    }

    public function approve($id)
    {
        if (Auth::user()->isManager())
        {
            Expense::find($id)->update(['is_approved' => 1]);

            return redirect('/expenses')->with('success', 'Expense updated!');
        }
    }
}
