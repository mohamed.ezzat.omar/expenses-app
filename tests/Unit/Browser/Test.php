<?php

namespace Browser;

use App\Models\User;
use ExpenseTest;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function testIndex()
    {
        $admin = User::find(1);
        $this->browse(function (Browser $browser) use ($admin) {
            $browser->loginAs($admin);
            $browser->visit(route('admin.expense.index'));
            $browser->assertRouteIs('admin.expense.index');
        });
    }
}
