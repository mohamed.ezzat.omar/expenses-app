<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DeleteExpenseTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function unauthorized_users_may_not_delete_expenses()
    {
        $expense = create('App\Models\Expense');

        $this->delete("expenses/$expense->id")->assertRedirect('/login');
    }

}
