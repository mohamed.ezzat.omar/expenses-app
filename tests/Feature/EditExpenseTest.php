<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EditExpenseTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function authorized_users_can_edit_expense()
    {
        $expense = $this->editExpense();

        $this->get("expenses/$expense->id/edit")
            ->assertSee($expense->name)
            ->assertSee($expense->amount);

        $response = $this->makeNewExpense($expense->id, [
            'name' => 'new expense',
            'amount' => 10
        ]);

        $this->get($response->headers->get('Location'))
            ->assertSee('new expense')
            ->assertSee('10');
    }

    /** @test */
    function a_expense_requires_a_name()
    {
        $expense = $this->editExpense();

        $this->makeNewExpense($expense->id, [
            'name' => '',
            'amount' => 50
        ])->assertSessionHasErrors('name');
    }

    /** @test */
    function a_post_requires_a_body()
    {
        $post = $this->editExpense();

        $this->makeNewExpense($post->id, [
            'name' => 'new expense',
            'amount' => null,
        ])->assertSessionHasErrors('amount');
    }

    /**
     * Edit Post
     * @param null $id
     * @return mixed
     */
    protected function editExpense($id = null)
    {
        $this->signIn();
        return create('App\Models\Expense', ['user_id' => $id ?: auth()->id()]);
    }


    /**
     * Make new expense
     * @param $id
     * @param array $array
     * @return TestResponse|\Illuminate\Testing\TestResponse
     */
    protected function makeNewExpense($id, array $array)
    {
        return $this->post("expenses/$id/update", $array);
    }
}
