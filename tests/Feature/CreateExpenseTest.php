<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateExpenseTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function guests_may_not_create_expense()
    {
        $this->get('/expenses')
            ->assertRedirect('/login');

        $this->get('/expenses/create')
            ->assertRedirect('/login');

        $this->post('/expenses')
            ->assertRedirect('/login');
    }


    /** @test */
    function an_authenticated_user_can_create_new_expense()
    {
        $this->signIn();

        $expense = make('App\Models\Expense');

        $response = $this->post('/Expenses', $expense->toArray());

        $this->get($response->headers->get('Location'))
            ->assertSee($expense->name)
            ->assertSee($expense->amount);
    }

    /** @test */
    function a_expense_requires_a_name()
    {
        $this->createExpense(['name' => null])
            ->assertSessionHasErrors('name');
    }

    /** @test */
    function a_expense_requires_a_amount()
    {
        $this->createExpense(['amount' => null])
            ->assertSessionHasErrors('amount');
    }

   /**
    * Create Expense
    * @param array $overrides
    * @return TestResponse|\Illuminate\Testing\TestResponse
    */
    protected function createExpense($overrides = [])
    {
        $this->signIn();

        $post = make('App\Models\Expense', $overrides);

        return $this->post('/expenses', $post->toArray());
    }
}
