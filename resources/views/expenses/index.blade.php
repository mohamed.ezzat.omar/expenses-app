@extends('welcome')

@section('main')
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <h1 class="display-3">Expenses</h1>
            <button type="button" class="btn btn-success" onclick="window.location='{{ url("expenses/create") }}'">Add New Expense</button>
            <div style="margin-bottom: 20px"></div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Amount</td>
                    <td>Attachment</td>
                    <td>Date</td>
                    @if(auth()->user()->isManager())
                    <td colspan = 3>Actions</td>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($expenses as $expense)
                    <tr>
                        <td>{{$expense->id}}</td>
                        <td>{{$expense->name}}</td>
                        <td>{{$expense->amount}}</td>
                        <td> <a href="{{'storage/app/attachments/'.$expense->attachment}}
                            " >{{$expense->attachment}}</a></td>
                        <td>{{$expense->date}}</td>
                        <td>
                            @if(auth()->user()->isManager())
                                <a href="{{ route('expenses.edit',$expense->id)}}" class="btn btn-primary">Edit</a>
                            @endif
                        </td>
                        <td>
                            @if(auth()->user()->isManager())
                            <form action="{{ route('expenses.destroy', $expense->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                            @endif
                        </td>
                        <td>
                            @if($expense->is_approved == 0 && auth()->user()->isManager())
                                <form action="{{ route('expenses.approve', $expense->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-success" type="submit">Approve</button>
                                </form>
                            @endif
                        </td>


                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
            </div>
@endsection
