@extends('welcome')
@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Update a expense</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
            @endif
            <form method="post" action="{{ route('expenses.update', $expense->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">

                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value={{ $expense->name }} />
                </div>

                <div class="form-group">
                    <label for="last_name">Amount:</label>
                    <input type="text" class="form-control" name="amount" value={{ $expense->amount }} />
                </div>

                <div class="form-group">
                    <label for="email">Details:</label>
                    <input type="text" class="form-control" name="attachment" value={{ $expense->attachment }} />
                </div>
                <div class="form-group">
                    <label for="city">Date:</label>
                    <input type="date" class="form-control" name="date" value={{ $expense->date }} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
