# Mini Expenses Tracker Using laravel 8
A Mini Expenses Tracker solution using latest version of laravel V8.0, This application is to build the foundation for a expenses application. This application stores, manage and display expenses.

The implementation use Laravel’s migrations, models, ORM/Eloquent, route, controllers, validator, and view/blade

## Included features
- As less database calls as possible
- As generic as possible
- As clean as possible


## Tools To build This Solution:
- Laravel knowledge.
- PHP knowledge
- MySQL knowledge
- Html , Css , Js , Ajax knowledge
- Version Control (Git) Knowladge , For More Details please visit this link :
	- [Adding an existing project to GitLab using the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

### Step 1: Before Run application
- Open your terminal or command prompt, then copy/paste the command given:
- Git clone https://gitlab.com/mohamed.ezzat.omar/expenses-app.git  your-project-name
- cd your-project-name
- Composer install
- Configure your database connection.
- composer dump-autoload
- php artisan migrate



### Step 2: Once the above step is done, you can simply run this small demo by:

**http://yourserver/yourappname/public/expenses**



